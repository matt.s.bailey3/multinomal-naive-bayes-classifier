#include <vector>
#include <string>

class Class{

	public:
	double numberOfDocuments;
	double numberOfWords;
	std::string label;
    std::vector<std::vector<float> > feature_vectors;
	Class(std::string set_label, int bag_length);
};