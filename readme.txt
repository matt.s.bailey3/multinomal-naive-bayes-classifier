1. 	Name: Matthew Bailey Username: mbailey
2. 	C++
3.	The program creates a multinomial naive bayes classifier in order to classify a large testing set of documents against all other classes from a large training set

4. 	Compiled with Visual Studio C++ Express 2013 for Windows Desktop, Windows 7 is supported.

	This program requires the boost library in order to run properly. Boost may
	be downloaded for windows here: http://sourceforge.net/projects/boost/files/boost/
	Download and extract the folder to your program files directory, the end directory should look like:

	"C:\Program Files\boost\boost_1_59_0"

	Afterward, naviage to "C:\Program Files(x86)\Microsoft Visual Studio 12.0\Common7\Tools\Shortcuts" and Open "VS2013 Native Tools-Command Prompt"

	Change directory to the boost folder (cd "C:\Program Files\boost\boost_1_59_0") and enter the two following commands:

	.\bootstrap.bat
	.\b2

	Create a new project c++ win32 console application in Visual Studio and import the main.cpp, Class.cpp, and Class.h files to it. Right click the project icon on the right side of the screen right under the project solution and select properties.
	From there, navigate to Configuration Properties -> C/C++ -> General -> Additional Include Directories and set the path to "C:\Program Files\boost\boost_1_59_0
	Afterward, navigate to Configuration Properties -> C/C++ -> Linker -> General -> Additional Library Directories and set the path to "C:\Program Files\boost\boost_1_59_0\stage\lib"

	A compiled binary has also been included as well, functions in standard command prompt in Windows 7/8.

	The 20_newsgroups folder MUST be in same directory as the binary.
