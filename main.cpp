#include "Class.h"
#include <iostream>
#include <fstream>
#include <unordered_map>
#include <boost/filesystem.hpp>
#include <boost/algorithm/string.hpp>
#include <math.h>


int main(){
	int word_index = 0;
	int class_index = 0;
        int num_classes = 0;
	std::unordered_map<std::string, int> bag_of_words;
	std::vector<Class> trainingClasses;
	std::vector<Class> testingClasses;
        float totalTrainingDocuments = 0;
        float totalTestingDocuments = 0;
        float positiveMatch = 0;
	std::ifstream inFile;
	std::string input;
	for (boost::filesystem::recursive_directory_iterator i("."), end; i != end; ++i){
		if (!is_directory(i->path()) && i->path().string().find("20_newsgroups\\") != std::string::npos){
    		inFile.open(i->path().string());
    		while (inFile >> input){
    			input.erase(input.find_last_not_of(" \n\r\t\"'.<>?:;,*")+1);
    			input.erase(0, input.find_first_not_of(" \n\r\t\"'.<>?:;*,"));
    			boost::algorithm::to_lower(input);
    			if(input.size() < 5 || input.size() > 7 || input == "or" || input == "and" || input == "but" || input == "if" || input == "can" || input == "for" || input == "a" || input == "i" || input == "of" || input == "at" || input == "to" || input == "on" || input == "it" || input == "the" || input == "are" || input == "in" || input == "is" || input == "an")
    				continue;
    			if(bag_of_words.find(input) != bag_of_words.end())
    				continue;
    			else {
    				bag_of_words.insert(std::pair<std::string, int>(input,word_index));
    				word_index++;
    			}
    		}
    		inFile.close();
    	}
    }
for (boost::filesystem::recursive_directory_iterator i("."), end; i != end; ++i){
    	if (is_directory(i->path()) && i->path().string().find("20_newsgroups\\") != std::string::npos && i->path().string().size() > 16){
    		trainingClasses.push_back(Class(i->path().string(), word_index));
    		testingClasses.push_back(Class(i->path().string(), word_index));
            num_classes++;
    	}
    }
    for (boost::filesystem::recursive_directory_iterator i("."), end; i != end; ++i){
		if (!is_directory(i->path()) && i->path().string().find("20_newsgroups\\") != std::string::npos && i->path().string().size() > 16){
    		input = i->path().string();
			input.erase(input.find_last_of("\\"));
    		if (input != trainingClasses[class_index].label)
    			class_index++;
    		inFile.open(i->path().string());
    		if ((rand() % 1000) > 333){
                trainingClasses[class_index].feature_vectors.push_back(std::vector<float>(word_index, 0));
    			while (inFile >> input){
    				input.erase(input.find_last_not_of(" \n\r\t\"'.<>?:;,*")+1);
    				input.erase(0, input.find_first_not_of(" \n\r\t\"'.<>?:;*,"));
    				boost::algorithm::to_lower(input);
					if (input.size() < 5 || input.size() > 7 || input == "or" || input == "and" || input == "but" || input == "if" || input == "can" || input == "for" || input == "a" || input == "i" || input == "of" || input == "at" || input == "to" || input == "on" || input == "it" || input == "the" || input == "are" || input == "in" || input == "is" || input == "an")
    					continue;
    				trainingClasses[class_index].feature_vectors[trainingClasses[class_index].numberOfDocuments][bag_of_words[input]]++;
                    trainingClasses[class_index].numberOfWords++;
    			}
                trainingClasses[class_index].numberOfDocuments++;
                totalTrainingDocuments++;
    		}
    		else {
                testingClasses[class_index].feature_vectors.push_back(std::vector<float>(word_index, 0));
    			while (inFile >> input){
    				input.erase(input.find_last_not_of(" \n\r\t\"'.<>?:;,*")+1);
    				input.erase(0, input.find_first_not_of(" \n\r\t\"'.<>?:;*,"));
    				boost::algorithm::to_lower(input);
    				if(input == "or" || input == "and" || input == "but" || input == "if" || input == "can" || input == "for" || input == "a" || input == "i" || input == "of" || input == "at" || input == "to" || input == "on" || input == "it" || input == "the" || input == "are" || input == "in" || input == "is" || input == "an")
    					continue;
    				testingClasses[class_index].feature_vectors[testingClasses[class_index].numberOfDocuments][bag_of_words[input]]++;
                    testingClasses[class_index].numberOfWords++;
    			}
                testingClasses[class_index].numberOfDocuments++;
                totalTestingDocuments++;
    		}
    		inFile.close();
    	}

    }
    std::vector<float> priors(num_classes, 0);
    std::vector<std::vector<float>> likelihoods(num_classes, std::vector<float>(word_index, 0));
    std::vector<std::vector<float>> testResults(totalTestingDocuments, std::vector<float>(num_classes, 0));
    std::vector<float> scoreKeeper(num_classes, 0);
    for(int i = 0; i < num_classes; i++){
        priors[i] = trainingClasses[i].numberOfDocuments / totalTrainingDocuments;
        for(int j = 0; j < word_index; j++){
            int sumOfAllDocWords = 0;
            for (int k = 0; k < trainingClasses[i].numberOfDocuments; k++){
                sumOfAllDocWords += trainingClasses[i].feature_vectors[k][j];
            }
            likelihoods[i][j] = (sumOfAllDocWords + 1.0) / (trainingClasses[i].numberOfWords + word_index);
        }
    }
    for (int i = 0; i < num_classes; i++){
        for (int j = 0; j < testingClasses[i].numberOfDocuments; j++){
            for (int l = 0; l < num_classes; l++){
            testResults[i*testingClasses[i].numberOfDocuments+j][l] = log(priors[l]);
                for(int k = 0; k < word_index; k++){
                    testResults[i*testingClasses[i].numberOfDocuments+j][l] = testResults[i*testingClasses[i].numberOfDocuments+j][l] + (testingClasses[i].feature_vectors[j][k] * log(likelihoods[l][k]));
                }
                //std::cout << "Document: " << i*testingClasses[i].numberOfDocuments+j << " Has a: " << testResults[i*testingClasses[i].numberOfDocuments+j][l] << " Score of belonging to class: " << l << " While it's real class is: " << i << std::endl;
                scoreKeeper[l] = testResults[i*testingClasses[i].numberOfDocuments+j][l];
            }
            if (distance(scoreKeeper.begin(), max_element (scoreKeeper.begin(), scoreKeeper.end())) == i)
                positiveMatch++;
        }
    }
    std::cout << "THE TOTAL AVERAGE CLASSIFICATION ACCURACY OF ALL INDIVIDUAL DOCUMENTS WAS: " << (positiveMatch / totalTestingDocuments)*100.0 << "\%" << std::endl;

	return 0;	
}
